/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef RANDOMPASSWORDGENERATOR_H
#define RANDOMPASSWORDGENERATOR_H

#include <array>

/**
 * @brief The RandomPasswordGenerator class
 *
 * A clas that generate random password for actual USB key.
 * Internally Nitrokey maintains a session password valid
 * until you disconnect from the key (e.g. remove the key
 * from USB slot). That password is 24 char long max, and
 * allows any values in range 1-255 (not only ASCII).
 */
class RandomPasswordGenerator
{
    /**
     * @brief PASS_LENGHT it's a max length of the
     * password, that Nitrokey supports plus the trailing
     * \0 byte (it's internally represented as a
     * C-style string).
     */
    static constexpr std::size_t PASS_LENGHT = 25;

public:

    /**
     * @brief Type alias for the password representation.
     */
    using KeyPassword = std::array<unsigned char, PASS_LENGHT>;

    /**
     * @brief Constructs an instance of the generator.
     */
    RandomPasswordGenerator();

    /**
     * @brief Generates random password.
     * @return Random passowrd ready to be used with the
     * actual USB key.
     */
    KeyPassword generatePassword() const;
};

#endif // RANDOMPASSWORDGENERATOR_H
