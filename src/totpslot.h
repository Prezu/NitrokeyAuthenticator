/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef TOTPSLOT_H
#define TOTPSLOT_H

#include <cstdint>
#include <string>

#include <QMetaType>

/**
 * @brief TOTPSlot represents indivitual TOTP slot in
 * <a href="https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3">
 * Nitrokey PRO2</a>, or
 * <a href="https://puri.sm/products/librem-key/">LibremKey</a> USB key.
 */
class TOTPSlot
{
public:
    /**
     * @brief Default constructor.
     *
     * Required by QMetaType class: https://doc.qt.io/qt-5/custom-types.html
     */
    TOTPSlot() = default;

    /**
     * @brief Default destructor.
     *
     * Required by QMetaType class: https://doc.qt.io/qt-5/custom-types.html
     */
    ~TOTPSlot() = default;

    /**
     * @brief Default copy constructor.
     *
     * Required by QMetaType class: https://doc.qt.io/qt-5/custom-types.html
     */
    TOTPSlot(const TOTPSlot &) = default;

    /**
     * @brief Constructs instance of TOTP slot representation.
     * @param name Name of the slot
     * @param slotNumber Index of the slot on USB key
     */
    TOTPSlot(const std::string &name, std::uint8_t slotNumber)
        : _slotNumber{slotNumber}, _slotName{name} {}

    /**
     * @brief Returns slot index of this TOTP slot on the key.
     * @return Index of the slot on USB key.
     */
    std::uint8_t slotNumber() const { return _slotNumber; }

    /**
     * @brief Returns name of TOTP slot.
     * @return Name of the slot.
     */
    std::string slotName() const { return _slotName; }

    /**
     * @brief Compares 2 instances of TOTPCode.
     * @param rhs Right-hand-side of == operator.
     * @return True if both, slot name and slot number are equal in both
     * TOTPCode instances. False otherwise.
     */
    bool operator ==(const TOTPSlot &rhs) const {
        return (_slotName == rhs._slotName) && (_slotNumber == rhs._slotNumber);
    }

private:
    std::uint8_t _slotNumber;
    std::string _slotName;
};

Q_DECLARE_METATYPE(TOTPSlot)
#endif // TOTPSLOT_H
