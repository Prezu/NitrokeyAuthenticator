/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef TOTPCONVERTER_H
#define TOTPCONVERTER_H

#include <QString>
#include <map>

/**
 * @brief The TotpConverter class convert Base_32 format secret to hex secret.
 */
class TotpConverter
{
public:

    /**
     * @brief Constructs instance of TotpConverter.
     */
    TotpConverter();

    /**
     * @brief Converts Base32 string format to Hex string.
     * @param original A Base32-encoded secret.
     * @return String in Hex format.
     */
    QString convertBase32ToHex(const QString &original) const;

private:
    QString convertBase32ToBinary(const QString &original) const;
    QString convertBinaryToHex(const QString &original) const;

    static constexpr int BIN_NUM_WIDTH = 5;
    static constexpr int BASE32_SEGMENT_LENGHT = 8;
    static constexpr int BIN_BASE = 2;
    static constexpr int HEX_BASE = 16;
    static constexpr int HEX_NUM_WIDTH = 2;

    /**
     * @brief RFC 4648 Base32 alphabet.
     */
    const std::map<char, int> B32_MAPPING =
    {
        { 'A', 0 }, { 'B', 1 }, { 'C', 2 }, { 'D', 3 }, { 'E', 4 },
        { 'F', 5 }, { 'G', 6 }, { 'H', 7 }, { 'I', 8 }, { 'J', 9 },
        { 'K', 10 }, { 'L', 11 }, { 'M', 12 }, { 'N', 13 }, { 'O', 14 },
        { 'P', 15 }, { 'Q', 16 }, { 'R', 17 }, { 'S', 18 }, { 'T', 19 },
        { 'U', 20 }, { 'V', 21 }, { 'W', 22 }, { 'X', 23 }, { 'Y', 24 },
        { 'Z', 25 }, { '2', 26 }, { '3', 27 }, { '4', 28 }, { '5', 29 },
        { '6', 30 }, { '7', 31 }
    };
};

#endif // TOTPCONVERTER_H
