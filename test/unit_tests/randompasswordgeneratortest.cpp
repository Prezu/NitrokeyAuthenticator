﻿/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gtest/gtest.h"

#include "randompasswordgenerator.h"
#include <set>

constexpr std::size_t PASS_LENGHT = 25;
constexpr int ITERATIONS_NO = 100;

TEST(RandomPasswordGeneratorTest, LastElementIsZero) {
    // Given
    RandomPasswordGenerator generator;

    for ( int i=0; i<ITERATIONS_NO; ++i ) {
        // When
        auto password = generator.generatePassword();

        // Then
        ASSERT_EQ(password[PASS_LENGHT-1], 0);
    }
}

TEST(RandomPasswordGeneratorTest, ElementAreNotZeros) {
    // Given
    RandomPasswordGenerator generator;

    for ( int i=0; i<ITERATIONS_NO; ++i ) {
        // When
        auto password = generator.generatePassword();

        // Then
        for ( std::size_t element=0; element<PASS_LENGHT-1; ++element ){
           ASSERT_NE(password[element], 0);
        }
    }
}

TEST(RandomPasswordGeneratorTest, CheckIfPasswordsAreUnique) {
    // Given
    RandomPasswordGenerator generator;
    std::set <std::string> passwords;

    // Entropy of the password is supposed to so high, that
    // having duplicate passwords genetarted INTERATIONS_NO
    // times should not be even remotely possible. This is, what
    // we're testing here.

    // When
    for ( int i=0; i<ITERATIONS_NO; ++i ) {
        RandomPasswordGenerator::KeyPassword randomPassword = generator.generatePassword();
        constexpr std::size_t IDX_OF_ZERO_BYTE = randomPassword.size() - 1;
        std::string password;
        for ( std::size_t i=0; i < IDX_OF_ZERO_BYTE; ++i) {
            password += randomPassword[i];
        }
        passwords.insert(password);
    }

    // Then
    ASSERT_EQ(passwords.size(), ITERATIONS_NO);
}
