﻿/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gtest/gtest.h"

#include <QString>
#include "totpconverter.h"

struct TotpConverterParams
{
    TotpConverterParams(QString b32_secret, QString hex_secret)
        : b32_secret(b32_secret), hex_secret(hex_secret)
    {
    }
    QString b32_secret;
    QString hex_secret;
};

class TotpConverterTestForDiffrentSecrets :
    public testing::TestWithParam<TotpConverterParams> {

};

INSTANTIATE_TEST_SUITE_P(InputValuesForTotpConverterTests,
                         TotpConverterTestForDiffrentSecrets,
                         testing::Values(
                             TotpConverterParams(
                                 "4GNJO26KCYVYMGILLRKDICZXBOLZG4SU",
                                 "E19A976BCA162B86190B5C54340B370B97937254"),
                             TotpConverterParams(
                                 "QRUC63RXEY3SCAL6",
                                 "84682F6E37263721017E"),
                             TotpConverterParams(
                                 "5AAREGEK",
                                 "E80112188A"),
                             TotpConverterParams(
                                 "ZQN4SYDIGT3B2WF4GHKJZINYAVLMF3IV",
                                 "CC1BC9606834F61D58BC31D49CA1B80556C2ED15"),
                             TotpConverterParams(
                                 "VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5",
                                 "AD67573C1248DE306007DD26CAEF65B2A237491D"),
                             TotpConverterParams(
                                 "RXH3AUXY5MAZSMDHOH5PLA46HHJK4GWMFMULH3UUTZGPUZ6FE7NQ",
                                 "8DCFB052F8EB0199306771FAF5839E39D2AE1ACC2B28B3EE949E4CFA67C527DB"),
                             TotpConverterParams(
                                 "IFBEG",
                                 "414243"),
                             TotpConverterParams(
                                 "IFBEGRA",
                                 "41424344")));

TEST_P(TotpConverterTestForDiffrentSecrets, CorrectSecretIsReturned) {
    // Given
    TotpConverter converter;

    // When
    const auto HEX_SECRET = converter.convertBase32ToHex(GetParam().b32_secret);

    // Then
    ASSERT_EQ(GetParam().hex_secret, HEX_SECRET);
}
